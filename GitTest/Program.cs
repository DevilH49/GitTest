﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Session 1
            Console.WriteLine("1. Git Init after fixing configurations of user.name & user.mail");
            Console.WriteLine("2. Git status can help us to find what`s going on our repository.");
            Console.WriteLine("3. Git add will add some files into the repo with filename or . as a complete selection.");
            Console.WriteLine("4. Git commit with -m verb will create a new commit for your current editions.");
            Console.WriteLine("5. Git remote add remote-name remote-url will set a location for pushing some files.");
            Console.WriteLine("6. Git remote remove remote-name will remove the controller and git remote will show you list of your added remotes.");
            Console.WriteLine("7. Git push --set-upstream remote-name branch-name(master) for first time and git push remote-name will push data to Git-URL.");

            //Session 2
            Console.WriteLine("8. Git log will show us all of committions of the respository.");
            Console.WriteLine("9. Git Show HEAD will show us edittions, SHA and Commit Name of the most recent commit! Note: Shift + Q will work as an escape key for your Git Bash editor.");
            Console.WriteLine("10. Git checkout HEAD filepath will revert all of our edittions to th original in our working directory. Note: You can use this, before commiting or you should be on staging area(just git add , not git commit).");
            Console.WriteLine("11. Git reset HEAD filepath will unstage one or multiple files from staging area. Note: You can use this, before commiting or you should be on staging area(just git add , not git commit).");
            Console.WriteLine("12. Git reset SHA(First Five Character) will remove a commit and let you switch between them. Note: We can find commit SHA in Git log ( for all ) and Git show HEAD ( for the latest commit )");

            Console.ReadKey();
            
        }
    }
}
